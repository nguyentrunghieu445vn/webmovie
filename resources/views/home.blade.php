@extends('layout.index')
@section('content')
<!-- Page Content -->
<!--Movie-->

    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading" style="background-color:#337AB7; color:white;" >
                <h2 style="margin-top:0px; margin-bottom:0px;text-transform: uppercase;"> Movie</h2>
            </div>

            <div class="panel-body">
                <!-- item -->
                <div class="row">
                    <div class="col-sm-4 col-xs-6 col-md-3">
                        <a href="javascript:;">
                            <img class="img-responsive" src="image/320x500.png" alt="">
                        </a>
                        <div class="block_wrapper">
                            <div class="movie-thumbnail">

                            </div>
                            <div class="movie_meta des_wrap">
                                <div class="des_wrap">Tên phim (tieng viet)</div>
                                <span class="movie_title_2 des_wrap">Tên phim (tieng anh)</span>
                                <span class="movie_title_chap">phut</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3">
                        <a href="javascript:;">
                            <img class="img-responsive" src="image/320x500.png" alt="">
                        </a>
                        <h4>Tên phim (tieng viet)</h4>
                        <p>Tên phim (tieng anh)</p>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3">
                        <a href="javascript:;">
                            <img class="img-responsive" src="image/320x500.png" alt="">
                        </a>
                        <h4>Tên phim (tieng viet)</h4>
                        <p>Tên phim (tieng anh)</p>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3">
                        <a href="javascript:;">
                            <img class="img-responsive" src="image/320x500.png" alt="">
                        </a>
                        <h4>Tên phim (tieng viet)</h4>
                        <p>Tên phim (tieng anh)</p>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3">
                        <a href="javascript:;">
                            <img class="img-responsive" src="image/320x500.png" alt="">
                        </a>
                        <h4>Tên phim (tieng viet)</h4>
                        <p>Tên phim (tieng anh)</p>
                    </div>

                </div>
                <!-- end item -->
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" style="background-color:#337AB7; color:white;" >
                <h2 style="margin-top:0px; margin-bottom:0px;text-transform: uppercase;"> Movie</h2>
            </div>

            <div class="panel-body">
                <!-- item -->
                <div class="row">
                    <div class="col-sm-4 col-xs-6 col-md-3">
                        <a href="javascript:;">
                            <img class="img-responsive" src="image/320x500.png" alt="">
                        </a>
                        <div class="block_wrapper">
                            <div class="movie-thumbnail">

                            </div>
                            <div class="movie_meta des_wrap">
                                <div class="des_wrap">Tên phim (tieng viet)</div>
                                <span class="movie_title_2 des_wrap">Tên phim (tieng anh)</span>
                                <span class="movie_title_chap">phut</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3">
                        <a href="javascript:;">
                            <img class="img-responsive" src="image/320x500.png" alt="">
                        </a>
                        <h4>Tên phim (tieng viet)</h4>
                        <p>Tên phim (tieng anh)</p>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3">
                        <a href="javascript:;">
                            <img class="img-responsive" src="image/320x500.png" alt="">
                        </a>
                        <h4>Tên phim (tieng viet)</h4>
                        <p>Tên phim (tieng anh)</p>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3">
                        <a href="javascript:;">
                            <img class="img-responsive" src="image/320x500.png" alt="">
                        </a>
                        <h4>Tên phim (tieng viet)</h4>
                        <p>Tên phim (tieng anh)</p>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3">
                        <a href="javascript:;">
                            <img class="img-responsive" src="image/320x500.png" alt="">
                        </a>
                        <h4>Tên phim (tieng viet)</h4>
                        <p>Tên phim (tieng anh)</p>
                    </div>

                </div>
                <!-- end item -->
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" style="background-color:#337AB7; color:white;" >
                <h2 style="margin-top:0px; margin-bottom:0px;text-transform: uppercase;"> Movie</h2>
            </div>

            <div class="panel-body">
                <!-- item -->
                <div class="row">
                    <div class="col-sm-4 col-xs-6 col-md-3">
                        <a href="javascript:;">
                            <img class="img-responsive" src="image/320x500.png" alt="">
                        </a>
                        <div class="block_wrapper">
                            <div class="movie-thumbnail">

                            </div>
                            <div class="movie_meta des_wrap">
                                <div class="des_wrap">Tên phim (tieng viet)</div>
                                <span class="movie_title_2 des_wrap">Tên phim (tieng anh)</span>
                                <span class="movie_title_chap">phut</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3">
                        <a href="javascript:;">
                            <img class="img-responsive" src="image/320x500.png" alt="">
                        </a>
                        <h4>Tên phim (tieng viet)</h4>
                        <p>Tên phim (tieng anh)</p>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3">
                        <a href="javascript:;">
                            <img class="img-responsive" src="image/320x500.png" alt="">
                        </a>
                        <h4>Tên phim (tieng viet)</h4>
                        <p>Tên phim (tieng anh)</p>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3">
                        <a href="javascript:;">
                            <img class="img-responsive" src="image/320x500.png" alt="">
                        </a>
                        <h4>Tên phim (tieng viet)</h4>
                        <p>Tên phim (tieng anh)</p>
                    </div>
                    <div class="col-sm-4 col-xs-6 col-md-3">
                        <a href="javascript:;">
                            <img class="img-responsive" src="image/320x500.png" alt="">
                        </a>
                        <h4>Tên phim (tieng viet)</h4>
                        <p>Tên phim (tieng anh)</p>
                    </div>

                </div>
                <!-- end item -->
            </div>
        </div>
    </div>

<!-- /.row -->

<!-- end Page Content -->
<!-- end Page Content -->
@endsection