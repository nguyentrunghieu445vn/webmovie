@extends('layout.index')
@section('content')


<div class="col-md-8">
    <div class="panel panel-default">
        <div class="panel-heading" style="background-color:#337AB7; color:white;" >
            <h2 style="margin-top:0px; margin-bottom:0px;text-transform: uppercase;">Đăng Ký</h2>
        </div>

        <div class="panel-body">
            <!-- item -->
            <div class="row">
                <form action="" method="post">
                    <table>
                        <tr>
                            <td>
                                <label>Tài khoản</label>
                                <input class="form-control" type="text" name="username" required="required"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Mật khẩu</label>
                                <input class="form-control" type="password" name="password" required="required"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align:center;">
                                <input type="submit" class="btn btn-primary" name="login" value="Đăng Nhập" />
                                <p><?php echo @$error; ?></p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align:right;">
                                <a href="index.php?action=forget-pass" class="btn btn-default btn-xs">Quên mật khẩu</a>
                            </td>
                        </tr>
                    </table>
                    <p style="text-align:right;"><a href="index.php"><span class="btn btn-warning">Trở về trang chủ</span></a></p>
                </form>

            </div>
            <!-- end item -->
        </div>
    </div>
</div>
@endsection
