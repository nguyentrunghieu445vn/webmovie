</div>
<!-- Footer -->
<footer>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="logo_web">
                        <h3><a href="javascript:;"><img src="#" alt="logo"/></a></h3>
                    </div>
                    <div class="col-md-12">
                        <p>Copyright &copy; Your Website 2014</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="footer_link col-md-4">
                        <h3 class="footer-link-head">Phim Mới</h3><a href="phim-le/" title="Phim lẻ mới">Phim lẻ mới</a>
                        <a href="phim-bo/" title="Phim bộ mới">Phim bộ mới</a>
                        <a href="phim-chieu-rap/" title="Phim chiếu rạp 2013">Phim chiếu rạp</a>
                        <a href="phim-kinh-dien/" title="Phim kinh điển hay">Phim kinh điển</a>
                        <a href="trailer/" title="Trailer phim sắp chiếu">Phim sắp chiếu</a>
                    </div>
                    <div class="footer_link col-md-4">
                        <h3 class="footer-link-head">Phim Mới</h3><a href="phim-le/" title="Phim lẻ mới">Phim lẻ mới</a>
                        <a href="phim-bo/" title="Phim bộ mới">Phim bộ mới</a>
                        <a href="phim-chieu-rap/" title="Phim chiếu rạp 2013">Phim chiếu rạp</a>
                        <a href="phim-kinh-dien/" title="Phim kinh điển hay">Phim kinh điển</a>
                        <a href="trailer/" title="Trailer phim sắp chiếu">Phim sắp chiếu</a>
                    </div>
                    <div class="footer_link col-md-4">
                        <h3 class="footer-link-head">Phim Mới</h3><a href="phim-le/" title="Phim lẻ mới">Phim lẻ mới</a>
                        <a href="phim-bo/" title="Phim bộ mới">Phim bộ mới</a>
                        <a href="phim-chieu-rap/" title="Phim chiếu rạp 2013">Phim chiếu rạp</a>
                        <a href="phim-kinh-dien/" title="Phim kinh điển hay">Phim kinh điển</a>
                        <a href="trailer/" title="Trailer phim sắp chiếu">Phim sắp chiếu</a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer_contact">
                        <h3>Liên hệ quảng cáo:</h3>
                        <p>Email: phimmoi.net@gmail.com<br></p>
                        <span>IP của bạn:</span>
                        <p id="client-ipaddress" data-ip="113.22.115.46">113.22.115.46</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- end Footer -->
<!-- jQuery -->
<script src="js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script src="js/my.js"></script>
<script src="js/eclipse.js" type="text/javascript"></script>
<script>
    if ($(window).width() >= 768)
    {
        $('#eclipse4').eclipse({
            margin: 20,
            slidesToShow: 4,
            slidesToMove: 4
        });
    } else {
        $('#eclipse4').eclipse({
            margin: 20,
            slidesToShow: 1,
            slidesToMove: 1
        });
    }
</script>
