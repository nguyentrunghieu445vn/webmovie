<!-- Navigation -->
<nav class="navbar navbar-inverse" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand text_up_case" href="/">Trang Chủ</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#">Giới thiệu</a>
                </li>
                <li>
                    <a href="#">Liên hệ</a>
                </li>
            </ul>

            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>

            <ul class="nav navbar-nav pull-right">
                <li>
                    <a href="{{route('dangky')}}">Đăng ký</a>
                </li>
                <li>
                    <a href="#">Đăng nhập</a>
                </li>
                <li>
                    <a>
                        <span class ="glyphicon glyphicon-user"></span>
                    </a>
                </li>

                <li>
                    <a href="#">Đăng xuất</a>
                </li>

            </ul>
        </div>



        <!-- /.navbar-collapse -->
    </div>

    <!-- /.container -->
    <div class="menu_wrapper">
        <div class="container">
            <div class="row">
                <ul class="nav navbar-nav text_up_case">
                    <li>
                        <a href="javascript:;">Phim mới</a>
                    </li>
                    <li id="theloai">
                        <a href="javascript:;">Thể loai</a>
                        <div class="container-fluid content">
                            <div class="col-md-3">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="javascript:;">
                                            phim hành động
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim võ thuật
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim cổ trang
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim viễn tưởng
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="javascript:;">
                                            phim hành động
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim võ thuật
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim cổ trang
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim viễn tưởng
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="javascript:;">
                                            phim hành động
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim võ thuật
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim cổ trang
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim viễn tưởng
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="javascript:;">
                                            phim hành động
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim võ thuật
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim cổ trang
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim viễn tưởng
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li id="quocgia">
                        <a href="javascript:;">quốc gia</a> 
                        <div class="container-fluid content">
                            <div class="col-md-3">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="javascript:;">
                                            phim hành động
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim võ thuật
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim cổ trang
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim viễn tưởng
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="javascript:;">
                                            phim hành động
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim võ thuật
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim cổ trang
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim viễn tưởng
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="javascript:;">
                                            phim hành động
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim võ thuật
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim cổ trang
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim viễn tưởng
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="javascript:;">
                                            phim hành động
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim võ thuật
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim cổ trang
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim viễn tưởng
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;">Phim lẻ</a> 
                        <div class="container-fluid content">
                            <div class="col-md-3">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="javascript:;">
                                            phim hành động
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim võ thuật
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim cổ trang
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim viễn tưởng
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="javascript:;">
                                            phim hành động
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim võ thuật
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim cổ trang
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim viễn tưởng
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="javascript:;">
                                            phim hành động
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim võ thuật
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim cổ trang
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim viễn tưởng
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="javascript:;">
                                            phim hành động
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim võ thuật
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim cổ trang
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim viễn tưởng
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;">Phim bộ</a>
                        <div class="container-fluid content">
                            <div class="col-md-3">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="javascript:;">
                                            phim hành động
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim võ thuật
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim cổ trang
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim viễn tưởng
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="javascript:;">
                                            phim hành động
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim võ thuật
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim cổ trang
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim viễn tưởng
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="javascript:;">
                                            phim hành động
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim võ thuật
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim cổ trang
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim viễn tưởng
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="javascript:;">
                                            phim hành động
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim võ thuật
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim cổ trang
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            phim viễn tưởng
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;">Phim bộ</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

