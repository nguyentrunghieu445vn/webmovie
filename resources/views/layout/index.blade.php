<!--header-->
@include('layout.header')
<!--menu-->
@include('layout.menu')
<!--slide-->
@include('layout.slide')
<!--content-->
@yield('content')
<!--slide right-->
@include('layout.slideright')
<!--footer-->
@include('layout.footer')