@extends('layout.index')
@section('content')


<div class="col-md-8">
    <div class="panel panel-default">
        <div class="panel-heading" style="background-color:#337AB7; color:white;" >
            <h2 style="margin-top:0px; margin-bottom:0px;text-transform: uppercase;">Đăng Ký</h2>
        </div>

        <div class="panel-body">
            <!-- item -->
            <div class="row">
                <form action="" class="col-md-12" method="post">

                    <div class="form-group col-md-6">
                        <label>Họ và tên</label>
                        <input class="form-control" id="inputEmail3" type="text" name="hoten" required="required" />
                    </div>

                    <div class="form-group col-md-6">
                        <label>Tài khoản</label>
                        <input class="form-control" id="inputEmail3" type="text" name="username_dk" required="required" />
                    </div>

                    <div class="form-group col-md-6">
                        <label>Mật khẩu:</label>
                        <input class="form-control" id="inputEmail3" type="password" name="password_dk" required="required" />
                    </div>

                    <div class="form-group col-md-6">
                        <label>Nhập lại mật khẩu:</label>
                        <input class="form-control" id="inputEmail3" type="password" name="password_dk1" required="required" />
                        <label></label>
                    </div>

                    <div class="form-group col-md-6">
                        <label>SĐT:</label>
                        <input class="form-control" id="sdt" type="number" min="1" name="SDT" required="required" />
                    </div>

                    <div class="form-group col-md-6">
                        <label>Địa chỉ email:</label>
                        <input class="form-control" id="inputEmail3" type="email" name="email" required="required" />
                    </div>

                    <div class="form-group col-md-12">
                        <center>
                            <input class="btn btn-primary" type="submit" name="dangky" value="Đăng ký" />
                        </center>
                    </div>

                </form>

            </div>
            <!-- end item -->
        </div>
    </div>
</div>
@endsection
