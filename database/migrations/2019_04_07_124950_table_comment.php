<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('comment',function(Blueprint $table){
            $table->integer('id_movie')->unsigned();
            $table->integer('id_audience')->unsigned();
            $table->primary(array('id_movie','id_audience'));
            $table->foreign('id_movie')->references('id_movie')->on('movie');
            $table->foreign('id_audience')->references('id_audience')->on('audience');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('comment');
    }
}
