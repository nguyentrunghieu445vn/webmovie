<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableMovie extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('movie',function(Blueprint $table){
            $table->increments('id_movie')->unsigned();
            $table->integer('id_cast')->unsigned()->nullable();
            $table->integer('id_directors')->unsigned()->nullable();
            $table->integer('id_nation')->unsigned()->nullable();
            $table->integer('id_category')->unsigned()->nullable();
            $table->foreign('id_cast')->references('id_cast')->on('cast');
            $table->foreign('id_directors')->references('id_directors')->on('directors');
            $table->foreign('id_nation')->references('id_nation')->on('nation');
            $table->foreign('id_category')->references('id_category')->on('category');
            $table->string('movie_name')->nullable();
            $table->string('content')->nullable();
            $table->string('image')->nullable();
            $table->integer('time')->nullable();
            $table->string('quality',50)->nullable();
            $table->string('resolution')->nullable();
            $table->string('languages')->nullable();
            $table->integer('status')->nullable();
            $table->integer('year')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('movie');
    }
}
