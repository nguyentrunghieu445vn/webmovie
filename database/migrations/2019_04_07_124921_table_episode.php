<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableEpisode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('episode',function(Blueprint $table){
            $table->increments('id_episode')->unsigned();
            $table->string('episode_name')->nullable();
            $table->integer('id_movie')->unsigned();
            $table->foreign('id_movie')->references('id_movie')->on('movie');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('episode');
    }
}
