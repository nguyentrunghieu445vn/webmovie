<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTrailer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('trailer',function(Blueprint $table){
            $table->increments('id_trailer')->unsigned();
            $table->string('trailer_name');
            $table->unsignedInteger('id_movie');
            $table->foreign('id_movie')->references('id_movie')->on('movie');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('trailer');
    }
}
